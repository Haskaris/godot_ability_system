# UML
See [side information](#side-information) for extra glossary (non-related to this project)

## Managers
### AbilityManager:
An [AbilityManager](#abilitymanager) is the manager designed to manage every [Ability](#ability). This is the node you want to ask "Can I use X ability?". It will see if the [Ability](#ability) is in [Cooldown](#cooldown) and if the [AbilityRequirement](#abilityrequirement) are fully satisfied. Since it's the master of every [Ability](#ability), it can override any [AbilityRequirement](#abilityrequirement) if needed.

### AttributeManager:
An [AttributeManager](#attributemanager) is the manager designed to manage every [Attribute](#attribute) of the [Entity](#entity) it's sticked to. It's like an interface, you should never interact directly with [Attribute](#attribute).

### EffectManager:
An [EffectManager](#effectmanager) is the manager designed to manage every [Effect](#effect) of [Attribute](#attribute). It have to be a child of an [AttributeManager](#attributemanager) since it will only interact with [Attribute](#attribute), you don't want to get an [Effect](#effect) if there is no corresponding [Attribute](#attribute).

### InOutManager:
An [InOutManager](#inoutmanager) is the manager designed to manage every [Income](#income) and [Outcome](#outcome) of the [Entity](#entity). It's not a child of an [AttributeManager](#attributemanager) but a child of the main [Entity](#entity) of the system since you can cast a spell without any [Attribute](#attribute).

## Abilities
### Ability:
An [Ability](#ability) is a group of [AbilityApplication](#abilityapplication). It's the main goal of the [Ability](#ability).

    Example:
        - Jump
        - Throw a fire ball
        - Dash then TP

### AbilityApplication:
An [AbilityApplication](#abilityapplication) is a precise description of part of an [Ability](#ability). Every application can be played automatically or can be played on an [Event](#event).

    Example for:
        - Jump:
            - Do jump: push the player along the normal
        - Throw a fire ball:
            - Perpare the spell: slow down the player, reduce the mana
            - Fire the fire ball: create a fire ball
        - Dash then TP:
            - Channel spell: stop the player, start a channel
            - Dash: dash the player, reduce his mana since he successfully started the spell
            - TP: teleport back the player 

### AbilityRequirement:
An [AbilityRequirement](#abilityrequirement) is an [Attribute](#attribute) requirement to start an [AbilityApplication](#abilityapplication).

    Example:
        - Need a specific amount of Mana

## Attributes
### Attribute:
An [Attribute](#attribute) is a number representing something on an [Entity](#entity) that interact with an [Ability](#ability).
    Example:
        - Health
        - Amount of ammo

## Modifiers
### Modifier:
A [Modifier](#modifier) is an abstract class (should not be used) representing a scaling of a value.

### InfiniteModifier:
An [InfiniteModifier](#infinitemodifier) is a [Modifier](#modifier) that will never stop if it is cleaned or removed.

    Example:
        - Permanent taking double damage

### DurationModifier:
A [DurationModifier](#durationmodifier) is a [Modifier](#modifier) that last in time. It can be cleaned, removed or reseted.

    Example:
        - Double every healing for 2.5s

## Effects
### Effect:
An [Effect](#effect) is an abstract class (should not be used) representing a modification on an [Attribute](#attribute).

### InfiniteEffect:
An [InfiniteEffect](#infiniteeffect) is an [Effect](#effect) that will never stop except if it is cleaned or removed.

    Example:
        - Passive healing

### DurationEffect:
A [DurationEffect](#durationeffect) is an [Effect](#effect) that last in time. It can be cleaned, removed or reseted.

    Example:
        - Fire

# Side information
### Cooldown:
A [Cooldown](#cooldown) is when you can't fire an [Ability](#ability) because it's recharging.

### Entity:
An [Entity](#entity) can be anything on what you want your [Ability](#ability), [Attribute](#attribute), etc. to interact with.

### Event:
An [Event](#event) is something external, it can be a player action like pressing a key, it can be an ingame action like getting hit by something.

### Income:
[Income](#income) is every [Attribute](#attribute) relative data input like damage, healing, etc.

### Outcome:
[Outcome](#outcome) is every [Attribute](#attribute) relative data outcome like damage, healing, etc.
