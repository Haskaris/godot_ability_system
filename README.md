# What is GodotAbilitySystem ?
GodotAbilitySystem is an addon developped by me for Godot 4.0, the goal of this addon is to be able to create easily any spells you can think of.

# How to install?
Copy `godot_abilities` into your `addons` repertory. If there is no `addons` repertory in your project, just create one.

# Can I use the project/addon?
Feel free to use everything you want here!
