## Ability class is the container of every `AbilityApplication`
##
## An Ability can be child of a node which will be the caster
##     or a child of an AbilityManager
class_name Ability
extends GodotAbilitySystem

signal on_clear()
signal on_cast()
signal on_application()
signal on_cooldown_window(ability_application_cooldown_window)
signal on_window(ability_application_window)
signal on_cooldown(ability_application_cooldown)
signal on_cooldown_ended()
signal on_ready()

## Signal reserved to display the cast/channel bar
signal on_channel_started()
signal on_channel_stopped()
signal on_channel_updated(new_value)

@export var id: String
@export var icon: Texture
@export_multiline var description: String

var _ability_application: Array = []
var _attribute_manager: AttributeManager
var _current_application: int = 0

@onready var parent: Node = get_parent()

func _ready() -> void:
	super._ready()
	
	tickable = false
	
	var _is_valid: bool = true
	
	for c in get_children():
		if not _is_valid:
			break
		if not c is AbilityApplication:
			if c is AttributeManager:
				if not is_instance_valid(_attribute_manager):
					_attribute_manager = c
				else:
					_is_valid = false
			else:
				_is_valid = false
		else:
			_ability_application.push_back(c)
	
	assert(_is_valid and (_ability_application.size() > 0), "An Ability need at least one \
	AbilityApplication, can have at most one AttributeManager and doesn't support other child")
	
	assert(id != "", "Id is required")

func init_ability():
	for a in _ability_application:
		a.init_ability_application()



## Public functions ################################################################################

func clear_ability() -> void:
	for c in _ability_application:
		c.clear_application()
	if is_instance_valid(_attribute_manager):
		_attribute_manager.clear_attributes()

func do_application() -> void:
	_ability_application[_current_application].do_cast()

func change_application() -> void:
	_current_application = (_current_application + 1) % _ability_application.size()

func get_caster() -> Node:
	if parent is Manager:
		return parent.get_entity()
	else:
		return parent

func get_remaining_cooldown() -> float:
	var to_return: float = _ability_application[_current_application].get_remaining_cooldown()
	return to_return

func get_requirements() -> Dictionary:
	return _ability_application[_current_application].get_requirements()

func get_status() -> int:
	var to_return: int = _ability_application[_current_application].get_status()
	return to_return

func is_available() -> bool:
	var to_return: bool = false
	var current_application: AbilityApplication = _ability_application[_current_application]
	# The current application is ready
	to_return = (current_application.get_status() == AbilityApplication.STATUS.READY)
	# There is a window
	if current_application.has_window():
		# There is another application
		if _current_application < _ability_application.size():
			# The current application is done but there is a window for the next application
			to_return = to_return or (current_application.get_status() == AbilityApplication.STATUS.WINDOW)
	return to_return

func is_requirement_ok() -> bool:
	var current_application: AbilityApplication = _ability_application[_current_application]
	var requirements: Dictionary = current_application.get_requirements()
	var requirements_are_ok: bool = true
	for k in requirements.keys():
		if requirements[k].use_min:
			if not _attribute_manager.get_attribute_value(k) >= requirements[k].min:
				requirements_are_ok = false
		if not requirements_are_ok:
			return requirements_are_ok
		if requirements[k].use_max:
			if not _attribute_manager.get_attribute_value(k) <= requirements[k].max:
				requirements_are_ok = false
		if not requirements_are_ok:
			return requirements_are_ok
	return true
