## Description of an ability
## You can see it as a cast of a multicast ability
class_name AbilityApplication
extends GodotAbilitySystem

enum STATUS {
	READY,
	CAST,
	APPLICATION,
	COOLDOWN_WINDOW,
	WINDOW,
	COOLDOWN,
}

## The ID of the Ability Application
@export var id: String = ""
## The cast duration of the Ability Application cast
@export var cast_duration: float = 0.0
## The duration of an Ability Application
@export var application_duration: float = 0.0
## The cooldown BEFORE the window appears
@export var cooldown_window_duration: float = 0.0
## The time during which the window will remain open
@export var window_duration: float = 0.0
## The cooldown after the Ability Application
@export var cooldown_duration: float = 0.0
## Not used so far
@export var free_spawnable_child: bool = true

## The Ability which is calling this application
var ability: Node
## The caster of this application
var caster: Node

@onready var _current_status: STATUS = STATUS.READY
@onready var _process_timer: float = 0.0
@onready var _physics_process_timer: float = 0.0
@onready var _requirements: Dictionary = {}
@onready var _timer: Timer = Timer.new()

@onready var waiting_reload: bool = false

var _child: Array = []

func _ready() -> void:
	super._ready()
	
	tickable = false
	
	set_process(true)
	
	for c in get_children():
		if c is AbilityRequirement:
			_requirements[c.attribute_id] = {
				"use_min": c.use_min_required,
				"min": c.min_value_required,
				"use_max": c.use_max_required,
				"max": c.max_value_required,
			}
		else:
			_child.push_back(c.duplicate())
			c.queue_free()

func _process(delta: float) -> void:
	if _current_status == STATUS.CAST:
		_process_timer += delta
		var ratio: float = ((_timer.wait_time - _timer.time_left) / _timer.wait_time) * 100.0
		ability.emit_signal(StringName("on_channel_updated"), ratio)
	if _current_status == STATUS.APPLICATION:
		pass
	# Reload the application
	if _current_status == STATUS.READY:
		if waiting_reload:
			_process_timer += delta
			if _process_timer >= cooldown_duration:
				waiting_reload = false
				do_ready()

func _physics_process(delta: float) -> void:
	if _current_status == STATUS.APPLICATION:
		var result: bool = await _do_application()
		_physics_process_timer += delta
		if _physics_process_timer >= application_duration:
			if result:
				do_cooldown_window()
			set_physics_process(false)

func init_ability_application() -> void:
	ability = get_parent()
	caster = ability.get_caster()
	
	_timer.one_shot = true
	add_child(_timer)
	do_ready()



## Public functions ################################################################################

func clear_application() -> void:
	set_process(false)
	set_physics_process(false)
	_current_status = STATUS.READY
	_timer.stop()
	_clear_timer()

func do_ready() -> void:
	ability.emit_signal(StringName("on_ready"))
	_current_status = STATUS.READY
	_clear_timer()
	_do_ready()

func do_cast() -> void:
	ability.emit_signal(StringName("on_cast"))
	_current_status = STATUS.CAST
	# cast_duration > 0.0
	if _clear_timer(cast_duration):
		ability.emit_signal(StringName("on_channel_started"))
		if await _do_cast():
			_timer.connect("timeout", do_application)
			_timer.start()
	else:
		do_application()

func do_application() -> void:
	ability.emit_signal(StringName("on_channel_stopped"))
	ability.emit_signal(StringName("on_application"))
	_current_status = STATUS.APPLICATION
	if application_duration == 0:
		if await _do_application():
			do_cooldown_window()
	else:
		set_physics_process(true)

func do_cooldown_window() -> void:
	ability.emit_signal(StringName("on_cooldown_window"), cooldown_window_duration)
	_current_status = STATUS.COOLDOWN_WINDOW
	ability.change_application()
	# cooldown_window_duration > 0.0
	if _clear_timer(cooldown_window_duration):
		_do_cooldown_window()
		_timer.connect("timeout", do_window)
		_timer.start()
	else:
		do_window()

func do_window() -> void:
	ability.emit_signal(StringName("on_window"), window_duration)
	_current_status = STATUS.WINDOW
	# window_duration > 0.0
	if _clear_timer(window_duration):
		if await _do_window():
			_timer.connect("timeout", do_cooldown)
			_timer.start()
	else:
		do_cooldown()

func do_cooldown() -> void:
	ability.emit_signal(StringName("on_cooldown"), cooldown_duration)
	_current_status = STATUS.COOLDOWN
	# cooldown_duration > 0.0
	if _clear_timer(cooldown_duration):
		_do_cooldown()
		_timer.connect("timeout", do_ready)
		_timer.start()
	else:
		do_ready()

func get_remaining_cooldown() -> float:
	var to_return: float = -1.0
	if _current_status == STATUS.COOLDOWN:
		to_return = _timer.time_left
	elif _current_status == STATUS.COOLDOWN_WINDOW and window_duration == 0.0:
		# If there is a cooldown window but no window since we
		# won't be able to use the windows, we add both cooldowns
		to_return = _timer.time_left + cooldown_duration
	return to_return

## Return a status
func get_status() -> int:
	return _current_status

func get_requirements() -> Dictionary:
	return _requirements

func has_window() -> bool:
	return window_duration > 0.0



## Private functions ###############################################################################

func _clear_timer(new_value: float = 0.0) -> bool:
	ability.emit_signal(StringName("on_channel_stopped"))
	if _timer.is_connected(StringName("timeout"), do_application):
		_timer.disconnect(StringName("timeout"), do_application)
	if _timer.is_connected(StringName("timeout"), do_ready):
		_timer.disconnect(StringName("timeout"), do_ready)
	if _timer.is_connected(StringName("timeout"), do_window):
		_timer.disconnect(StringName("timeout"), do_window)
	if _timer.is_connected(StringName("timeout"), do_cooldown):
		_timer.disconnect(StringName("timeout"), do_cooldown)
	if new_value <= 0.0:
		_timer.stop()
	else:
		_timer.wait_time = new_value
	_process_timer = 0.0
	_physics_process_timer = 0.0
	
	return new_value > 0.0


### Overide section ################################################################################
## Return true to know if we continue the normal flow

func _do_ready() -> void:
	pass

func _do_cast() -> bool:
	return true

func _do_application() -> bool:
	return true

## No return since it is a cooldown
func _do_cooldown_window() -> void:
	pass

func _do_window() -> bool:
	return true

## No return since it is a cooldown
func _do_cooldown() -> void:
	pass
