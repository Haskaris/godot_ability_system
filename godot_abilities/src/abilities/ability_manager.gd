## Manager of every ability
class_name AbilityManager
extends Manager

var _ability_list: Dictionary = {}

@onready var parent: Node = get_parent()

func _ready() -> void:
	super._ready()
	
	tickable = false
	
	for c in get_children():
		assert(c is Ability, "AbilityManager only support Ability children")
		_ability_list[c.id] = c

func init_ability_manager() -> void:
	for a_key in _ability_list.keys():
		_ability_list[a_key].init_ability()



## Public functions ################################################################################

func add_ability(id: String, ability: Ability) -> void:
	add_child(ability)
	_ability_list[id] = ability
	_ability_list[id].init_ability()

func can_start_ability(id: String) -> bool:
	if _ability_list.has(id):
		var to_return: bool = _ability_list[id].is_available() and _ability_list[id].is_requirement_ok()
		return to_return
	else:
		return false

func clear_ability(id: String) -> void:
	_ability_list[id].clear_ability()

func clear_all_abilities() -> void:
	for k in _ability_list.keys():
		_ability_list[k].clear_ability()

func do_ability(id: String) -> void:
	_ability_list[id].do_application()

func get_ability_remaining_cooldown(id: String) -> float:
	var to_return: float = _ability_list[id].get_remaining_cooldown()
	return to_return

func get_ability_requirements(id: String) -> Dictionary:
	var to_return: Dictionary = _ability_list[id].get_requirements()
	return to_return

func get_ability_status(id: String) -> int:
	if _ability_list.has(id):
		var to_return: int = _ability_list[id].get_status()
		return to_return
	else:
		return -1

func get_entity() -> Node:
	if parent is ActionManager:
		return parent.entity
	else:
		return parent
