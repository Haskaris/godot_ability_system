## Describe the requirement of an ability to be firered
class_name AbilityRequirement
extends GodotAbilitySystem

@export var attribute_id: String
@export var use_min_required: bool = true
@export var use_max_required: bool = false
@export var min_value_required: float = 0.0
@export var max_value_required: float = 0.0
