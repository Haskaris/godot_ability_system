extends Manager
class_name EffectManager

var effect_list: Dictionary = {}

func _ready() -> void:
	super._ready()
	
	tickable = false
	
	for c in get_children():
		assert(c is Effect, "EffectManager only support Effect children")
		effect_list[c.id] = c

func init_manager() -> void:
	for k in effect_list.keys():
		effect_list[k].init_effect()


## Public functions ################################################################################

func add_effect(effect: Effect) -> void:
	effect_list[effect.id] = effect
	add_child(effect)

func get_effect(id: String) -> Effect:
	assert(effect_list.has(id), "EffectManager doesn't have required effect")
	return effect_list.get(id)

func link_attribute(attribute_id: String) -> Attribute:
	var attribute_manager: Node = get_parent() # AttributeManager
	var to_return: Attribute = attribute_manager.get_attribute(attribute_id)
	return to_return

func modify_effect(id: String, change_value: float) -> float:
	assert(effect_list.has(id), "EffectManager doesn't have required effect")
	var attribute_to_modify: Attribute = effect_list.get(id)
	#TODO: Change the value of the attribute
	return 0.0

func clear_all_effects() -> void:
	for k in effect_list.keys():
		effect_list[k].clear_effect()

func clear_all_clearable_effects() -> void:
	for k in effect_list.keys():
		var effect: Effect = effect_list[k]
		if effect.clearable:
			effect.clear_effect()

func clear_effect(id: String) -> void:
	assert(effect_list.has(id), "EffectManager doesn't have required effect")
	effect_list[id].clear_effect()
