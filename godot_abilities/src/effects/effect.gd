## An Effect is a change on an Attribute
##
## For example:
##   +0.5 on attribute health <- Passive healing
##
class_name Effect
extends GodotAbilitySystem

@export var id: String
@export var attribute_id: String

## How much of value change in a second
@export var to_change: float = 0.0
## If the effect can be cleared
@export var clearable: bool = true

var _attribute: Attribute


signal effect_cleared()

func _ready() -> void:
	super._ready()
	
	assert(id != "", "Id is required")
	assert(attribute_id != "", "AttributeId is required")
	


func tick(_tick_number: int, tick_delta: float) -> void:
	apply_effect(tick_delta)


## Private functions ###############################################################################

func _pause_effect() -> void:
	tickable = false

func _resume_effect(_new_value: float) -> void:
	tickable = true



## Public functions ################################################################################

func apply_effect(delta: float = 0.0) -> void:
	if delta == 0:
		_attribute.change(to_change)
	else:
		_attribute.change(to_change * delta)

func clear_effect() -> void:
	emit_signal("effect_cleared")
	queue_free()

func init_effect() -> void:
	var effect_manager: Node = get_parent() # EffectManager
	_attribute = effect_manager.link_attribute(attribute_id)
	_attribute.connect(StringName("max_value_reached"), _pause_effect)
	_attribute.connect(StringName("current_value_updated"), _resume_effect)
