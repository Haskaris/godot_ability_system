## Main class of every managers
class_name Manager
extends GodotAbilitySystem


func _ready() -> void:
	super._ready()
	
	tickable = false
	
	for c in get_children():
		if not c is Manager:
			assert(c.id != "", "Every child of a manager that is not a manager should have an id")



## Private functions ###############################################################################



## Public functions ################################################################################

func has_by_id(child_id: String) -> bool:
	var to_return: bool = false
	for c in get_children():
		if c.id == child_id:
			to_return = true
			break
	return to_return

func get_by_id(child_id: String) -> Node:
	var to_return: Node = null
	for c in get_children():
		if c.id == child_id:
			to_return = c
			break
	assert(to_return != null,\
		"get_by_id can't return null, please be sure to use has_by_id method")
	return to_return
