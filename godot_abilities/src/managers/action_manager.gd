extends Manager
class_name ActionManager

var ability_manager: AbilityManager = null
var attribute_manager: AttributeManager = null
var status_manager: StatusManager = null
var in_out_manager: InOutManager = null

@onready var entity: Node = get_parent()

func _ready() -> void:
	super._ready()
	
	tickable = false
	
	var has_AbilityManager: bool = false
	var has_AttributeManager: bool = false
	var has_StatusManager: bool = false
	var has_InOutManager: bool = false
	
	assert(get_child_count() == 3,\
		"ActionManager require 3 childrens: AbilityManager, AttributeManager and StatusManager")
	
	for c in get_children():
		if c is AbilityManager:
				assert(not has_AbilityManager,\
					"ActionManager can not have more than 1 AbilityManager")
				has_AbilityManager = true
				ability_manager = c
		elif c is AttributeManager:
				assert(not has_AttributeManager,\
					"ActionManager can not have more than 1 AttributeManager")
				has_AttributeManager = true
				attribute_manager = c
		elif c is StatusManager:
				assert(not has_StatusManager,\
					"ActionManager cannot have more than 1 StatusManager")
				has_StatusManager = true
				status_manager = c
		elif c is InOutManager:
				assert(not has_InOutManager,\
					"ActionManager cannot have more than 1 StatusManager")
				has_InOutManager = true
				in_out_manager = c
	
	if has_AbilityManager:
		ability_manager.init_ability_manager()



## Public functions ################################################################################

func apply_input(attribute_id: String, input: float) -> void:
	var apply_input: float = input
	if is_instance_valid(in_out_manager):
		pass
		# Change input value
	if is_instance_valid(attribute_manager):
		attribute_manager.modify_attribute(attribute_id, apply_input)

func apply_status(status: Status) -> void:
	if is_instance_valid(status_manager):
		if is_instance_valid(status):
			status_manager.add_status(status)
	else:
		_handle_no_status_manager()

func use_ability(ability_id: String) -> void:
	if is_instance_valid(ability_manager):
		if is_instance_valid(status_manager):
			var status_ok = status_manager.is_tag_allowed("ability")
			if status_ok:
				_try_to_start_ability(ability_id)
			else:
				_handle_status_not_ok()
		else:
			_try_to_start_ability(ability_id)
	else:
		_handle_no_ability_manager()



## Private functions ###############################################################################

# Factorisation of code
func _try_to_start_ability(ability_id: String) -> void:
	# Check if the ability can be started
	var can_start_ability: bool = ability_manager.can_start_ability(ability_id)
	if can_start_ability:
		# Get requirements of ability
		var ability_requirements: Dictionary = ability_manager.get_ability_requirements(ability_id)
		var requirement_ok: bool = _handle_requirements(ability_requirements)
		if requirement_ok:
			ability_manager.do_ability(ability_id)
		else:
			_handle_requirements_not_ok()
	else:
		_handle_cannot_start_ability(ability_id)



## Custom handlers #################################################################################

func _handle_cannot_start_ability(ability_id: String) -> void:
	pass

func _handle_no_ability_manager() -> void:
	pass

func _handle_no_status_manager() -> void:
	pass

func _handle_requirements(requirements: Dictionary) -> bool:
	return true

func _handle_requirements_not_ok() -> void:
	pass

func _handle_status_not_ok() -> void:
	pass
