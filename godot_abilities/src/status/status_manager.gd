## StatusManager is a Manager for Status
## You use it to communicate with every Status of your Entity
class_name StatusManager
extends Manager

signal status_added(status)

var status: Dictionary = {}

func _ready() -> void:
	super._ready()
	
	tickable = false
	
	for c in get_children():
		assert(c is Status, "StatusManager only support Status children")
		status[c.id] = c



## Private functions ###############################################################################

func _end_status(id: String) -> void:
	status.erase(id)



## Public functions ################################################################################

## Set tickable to true on the status (activate the status)
func add_status(child: Status) -> void:
	if status.has(child.id):
		get_node(child.id).refresh_status(child)
	else:
		child.name = child.id
		add_child(child)
		status[child.id] = child
		child.tickable = true
		emit_signal(StringName("status_added"), child)

func clear_every_status() -> void:
	status.clear()
	for c in get_children():
		c.queue_free()

func clear_status(id: String) -> void:
	status[id].queue_free()
	status.erase(id)

func clear_tag(tag_id: String) -> void:
	var key_to_remove: Array = []
	for k in status.keys():
		if status[k].disableTagId == tag_id:
			status[k].queue_free()
			key_to_remove.push_back(k)
	for k in key_to_remove:
		status.erase(k)

func has_status(id: String) -> bool:
	var to_return: bool = status.has(id)
	return to_return

func is_tag_allowed(tag_id: String) -> bool:
	#for k in status.keys():
	#	if status[k].disableTagId == tag_id:
	#		return false
	return true

func refresh_status(id: String) -> void:
	status[id].refresh_status()
