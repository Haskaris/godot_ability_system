## A Status can be a crowd control, a silence, etc...
class_name Status
extends GodotAbilitySystem

signal status_cleared()
signal status_ended()
signal status_refreshed()

@export var id: String = ""
@export var displayed_name: String = ""
@export var duration: float = 0.0

#@export var disableTagId: String

@onready var _timer: float = 0.0
@onready var _player: Node3D = get_parent().owner as Node3D

func _ready() -> void:
	super._ready()
	
	tickable = false
	
	assert(get_child_count() == 0, "Status node can't have a child")
	assert(id != "", "Status need an id")
	
	if displayed_name == "":
		displayed_name = id


func tick(tick_number: int, tick_delta: float) -> void :
	apply_status(tick_delta)
	_timer += tick_delta
	if _timer >= duration:
		_timer = 0.0
		_end_status()



## Private functions ###############################################################################

func _end_status() -> void:
	emit_signal(StringName("status_ended"))
	get_parent()._end_status(id)
	queue_free()

func _custom_refresh(status: Node) -> void:
	pass


## Public functions ################################################################################

func apply_status(delta: float) -> void:
	pass

func clear_status() -> void:
	emit_signal(StringName("status_cleared"))
	queue_free()

func refresh_status(status_to_refresh_from: Node) -> void:
	emit_signal(StringName("status_refreshed"))
	_timer = 0.0
	_custom_refresh(status_to_refresh_from)
