## The main parent class of this addon
##
## We need a singleton to tick since some nodes aren't using
## PhysicProcess
class_name GodotAbilitySystem
extends Node

@export var tickable: bool = true:
	get:
		return tickable
	set(value):
		if value:
			if not Server.is_connected(StringName("tick"), tick):
				Server.connect(StringName("tick"), tick)
		else:
			if Server.is_connected(StringName("tick"), tick):
				Server.disconnect(StringName("tick"), tick)
		tickable = value

func _ready() -> void:
	set_process(false)
	set_physics_process(false)
	
	if tickable:
		if not Server.is_connected(StringName("tick"), tick):
			Server.connect(StringName("tick"), tick)

func tick(tick_number: int, tick_delta: float) -> void:
	pass
