## Modifier is a class for modifier change on an attribute
##
## For example:
##   TODO
class_name Modifier
extends GodotAbilitySystem

@export var id: String
@export var attribute_id: String

@export var modify_up: float = 1.0
@export var modify_down: float = 1.0

@export var clearable: bool = true

var _attribute: Attribute

signal modifier_cleared()

func _ready() -> void:
	super._ready()
	
	tickable = false
	
	assert(is_instance_valid(id), "Id is required")
	assert(is_instance_valid(attribute_id), "AttributeId is required")
	
	var modifier_manager: Node = get_parent() # ModifierManager
	_attribute = modifier_manager.link_attribute(attribute_id)



## Private functions ###############################################################################



## Public functions ################################################################################

func apply_modification_up(value: float) -> float:
	return value * modify_up

func apply_modification_down(value: float) -> float:
	return value * modify_down

func clear_modifier() -> void:
	emit_signal("modifier_cleared")
	queue_free()
