extends Manager
class_name ModifierManager

var modifier_list: Dictionary = {}

func _ready() -> void:
	super._ready()
	
	tickable = false
	
	for c in get_children():
		assert(c is Modifier, "ModifierManager only support Modifier children")
		modifier_list[c.id] = c



## Public functions ################################################################################

func add_modifier(modifier: Modifier) -> void:
	modifier_list[modifier.id] = modifier
	add_child(modifier)

func get_modifier(id: String) -> Modifier:
	assert(modifier_list.has(id), "ModifierManager doesn't have required modifier")
	return modifier_list.get(id)

func link_attribute(attribute_id: String) -> Attribute:
	var attribute_manager: Node = get_parent() # AttributeManager
	var to_return: Attribute = attribute_manager.get_attribute(attribute_id)
	return to_return

func modify_change_value(attribute_id: String, change_value: float) -> float:
	var scale: float = 1.0
	if change_value != 0:
		for k in modifier_list.keys():
			if modifier_list[k].attribute_id == attribute_id:
				if change_value > 0:
					scale = scale * modifier_list[k].modify_up
				else:
					scale = scale * modifier_list[k].modify_down
	return change_value * scale

func clear_all_modifiers() -> void:
	for k in modifier_list.keys():
		modifier_list[k].clear_effect()

func clear_all_clearable_modifiers() -> void:
	for k in modifier_list.keys():
		var modifier: Modifier = modifier_list[k]
		if modifier.clearable:
			modifier.clear_modifier()

func clear_modifier(id: String) -> void:
	assert(modifier_list.has(id), "ModifierManager doesn't have required modifier")
	modifier_list[id].clear_effect()
