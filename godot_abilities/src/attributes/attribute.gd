class_name Attribute
extends GodotAbilitySystem

signal current_value_updated(current_value: float)
signal min_value_reached()
signal max_value_reached()
signal min_value_updated(new_min_value: float, used: bool)
signal max_value_updated(new_max_value: float, used: bool)

@export var id: String

## If true the attribute will deal with integer
## Overwise it will use float
@export var use_int: bool = false

@export var use_max: bool = true
@export var use_min: bool = true

## Default min value that the attribute can't go below
@export var default_min_value: float = 0.0
## Default man value that the attribute can't go above
@export var default_max_value: float = 0.0
## Default value; the initiatlization of the value
@export var default_value: float = 0.0

@export var linked_attribute: Array

var _current_value: float = 0.0
var _remaining_value: float = 0.0
var _linked_attribute: Dictionary = {}

@onready var min_value: float = default_min_value
@onready var max_value: float = default_max_value

func _ready() -> void:
	super._ready()
	
	tickable = false
	
	assert(id != "", "Id is required")
	
	if use_min and use_max:
		assert(min_value < max_value, "min_value can't be bigger or equals than max_value")
	
	update_value(default_value)


## Private functions ###############################################################################

func update_value(new_value: float) -> void:
	emit_signal("current_value_updated", _current_value)
	var new_value_clamped: float = new_value
	
	if use_min:
		new_value_clamped = max(new_value, min_value)
		
		if new_value_clamped <= min_value:
			emit_signal("min_value_reached")
	
	if use_max:
		new_value_clamped = min(new_value_clamped, max_value)
		
		if new_value_clamped >= max_value:
			emit_signal("max_value_reached")
	
	_current_value = new_value_clamped



## Public functions ################################################################################

#Same as reduce or add
func change(to_change: float) -> float:
	if to_change < 0:
		reduce(-to_change)
	elif to_change > 0:
		add(to_change)
	return _current_value

#Reduce(5) -> current value = current value - 5 
#Usefull if you want to override damage behavior (ie: armor)
func reduce(to_reduce: float) -> void:
	var change_value: float = to_reduce
	if use_int:
		change_value = int(to_reduce)
		_remaining_value -= absf(to_reduce - change_value)
		if _remaining_value <= -1.0:
			change_value += 1.0
			_remaining_value += 1.0
	
	var result: float = _current_value - change_value
	update_value(result)

#Add(5) -> current value = current value + 5 
func add(to_add: float) -> void:
	var change_value: float = to_add
	if use_int:
		change_value = int(to_add)
		_remaining_value += absf(change_value - to_add)
		if _remaining_value >= 1.0:
			change_value += 1.0
			_remaining_value -= 1.0
	
	var result: float = _current_value + change_value
	update_value(result)

func clear_attribute() -> void:
	update_value(default_value)


### Getter functions ###############################################################################
func get_value() -> float:
	return _current_value


### Setter functions ###############################################################################
func set_min(new_min: float) -> void:
	min_value = new_min
	emit_signal("min_value_updated", min_value, use_min)

func set_max(new_max: float) -> void:
	max_value = new_max
	emit_signal("max_value_updated", max_value, use_max)

func set_use_min(using: bool) -> void:
	use_min = using
	emit_signal("min_value_updated", min_value, use_min)

func set_use_max(using: bool) -> void:
	use_max = using
	emit_signal("max_value_updated", max_value, use_max)
