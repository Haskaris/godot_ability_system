class_name AttributeManager
extends Manager

var attribute_list: Dictionary = {}
var effect_manager: EffectManager
var modifier_manager: ModifierManager

func _ready() -> void:
	super._ready()
	
	tickable = false
	
	for c in get_children():
		if c is Attribute:
			attribute_list[c.id] = c
		elif c is EffectManager:
			effect_manager = c
		elif c is ModifierManager:
			modifier_manager = c
		else:
			assert(false, "AttributeManager got a wrong child")
	effect_manager.init_manager()


## Public functions ################################################################################

func add_attribute_from_data(attribute_data: Dictionary) -> void:
	var new_child: Attribute = Attribute.new()
	new_child.id = attribute_data["id"]
	new_child.use_max = attribute_data["use_max"]
	new_child.use_min = attribute_data["use_min"]
	new_child.max_value = attribute_data["max_value"]
	new_child.min_value = attribute_data["min_value"]
	new_child.default_value = attribute_data["default_value"]
	new_child.linked_attribute = attribute_data["linked_attribute"]
	add_attribute(new_child)

func add_attribute(attribute: Attribute) -> void:
	attribute_list[attribute.id] = attribute
	add_child(attribute)

func get_attribute(id: String) -> Attribute:
	assert(attribute_list.has(id), "AttributeManager doesn't have required attribute")
	return attribute_list.get(id)

func set_attribute_value(id: String, new_value: float) -> void:
	assert(attribute_list.has(id), "AttributeManager doesn't have required attribute")
	return attribute_list.get(id).update_value(new_value)

func get_attribute_value(id: String) -> float:
	assert(attribute_list.has(id), "AttributeManager doesn't have required attribute")
	return attribute_list.get(id).get_value()

func has_attribute(id: String) -> bool:
	return attribute_list.has(id)

func modify_attribute(id: String, change_value: float) -> float:
	assert(attribute_list.has(id), "AttributeManager doesn't have required attribute")
	var attribute_to_modify: Attribute = attribute_list.get(id)
	var to_return: float = attribute_to_modify.change(change_value)
	return to_return

func clear_all_attributes() -> void:
	for k in attribute_list.keys():
		attribute_list[k].clear_attribute()

func clear_attribute(id: String) -> void:
	assert(attribute_list.has(id), "AttributeManager doesn't have required attribute")
	attribute_list[id].clear_attribute()
