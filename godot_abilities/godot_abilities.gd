@tool
extends EditorPlugin


func _not_enter_tree():
	## Manager #######################################################################################
	add_custom_type("Manager", "Node3D",\
		preload("src/managers/manager.gd"), null)
	add_custom_type("AbilityManager", "Node3D",\
		preload("src/abilities/ability_manager.gd"), null)
	add_custom_type("ActionManager", "Node3D",\
		preload("src/managers/action_manager.gd"), null)
	add_custom_type("AttributeManager", "Node3D",\
		preload("src/attributes/attribute_manager.gd"), null)
	add_custom_type("EffectManager", "Node3D",\
		preload("src/effects/effect_manager.gd"), null)
#	add_custom_type("InOutManager", "Node3D",\
#		preload("src/managers/in_out_manager.gd"), null)
	add_custom_type("ModifierManager", "Node3D",\
		preload("src/modifiers/modifier_manager.gd"), null)
	add_custom_type("StatusManager", "Node3D",\
		preload("src/status/status_manager.gd"), null)
	
	## Ability #######################################################################################
	add_custom_type("Ability", "Node3D",\
		preload("src/abilities/ability.gd"), null)
	add_custom_type("AbilityApplication", "Node3D",\
		preload("src/abilities/ability_application.gd"), null)
	add_custom_type("AbilityRequirement", "Node3D",\
		preload("src/abilities/ability_requirement.gd"), null)
	
	## Attribute #####################################################################################
	add_custom_type("Attribute", "Node3D",\
		preload("src/attributes/attribute.gd"), null)
		
	## Effect ########################################################################################
	add_custom_type("Effect", "Node3D",\
		preload("src/effects/effect.gd"), null)


func _not_exit_tree():
	## Manager #####################################################################################
	remove_custom_type("Manager")
	remove_custom_type("AbilityManager")
	remove_custom_type("ActionManager")
	remove_custom_type("AttributeManager")
	remove_custom_type("EffectManager")
#	remove_custom_type("InOutManager")
	remove_custom_type("ModifierManager")
	remove_custom_type("StatusManager")
	
	## Ability #######################################################################################
	remove_custom_type("Ability")
	remove_custom_type("AbilityApplication")
	remove_custom_type("AbilityRequirement")
	
	## Attribute #####################################################################################
	remove_custom_type("Attribute")
	
	## Effect ########################################################################################
	remove_custom_type("Effect")
